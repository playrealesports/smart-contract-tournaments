/*
 * AEI Tournaments
 *
 */

// To conserve gas, efficient serialization is achieved through Borsh (http://borsh.io/)
use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::BorshStorageKey;
use near_sdk::collections::UnorderedMap;
use near_sdk::json_types::U128;
use near_sdk::serde::{Deserialize, Serialize};
use near_sdk::{env, near_bindgen, AccountId, Promise};
use std::collections::HashMap;
use std::convert::{TryFrom};

type Place = u16;
type YoctoNear = u128;
type TournamentId = u32;

#[derive(BorshStorageKey, BorshSerialize)]
enum StorageKeys {
    Accounts,
}

#[derive(BorshSerialize, BorshDeserialize, Serialize, Deserialize)]
#[serde(crate = "near_sdk::serde")]
pub struct Winner {
    accounts: Vec<AccountId>,
    prize_multiplier_perk: f64,
    place: Place,
}

#[derive(BorshSerialize, BorshDeserialize, Serialize, Deserialize)]
#[serde(crate = "near_sdk::serde")]
pub struct JsonPrizePlace {
    player_position: Place,
    team_prize_base_amount: U128,
}

#[derive(BorshSerialize, BorshDeserialize, Serialize, Deserialize, Default)]
#[serde(crate = "near_sdk::serde")]
pub struct Tournament {
    tournament_id: TournamentId,
    total_prize_announced: YoctoNear,
    total_prize_reserved: YoctoNear,
    prize_payout_scheme: HashMap<Place, YoctoNear>,
    participant_list: Vec<AccountId>,
    winner_list: Vec<Winner>,
    status: String, // created, started, finished + cancelled
    source_wallet: String,
}

// Define the contract structure
#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize)]
pub struct Contract {
    tournaments: UnorderedMap<TournamentId, Tournament>,
}

// Define the default, which automatically initializes the contract
impl Default for Contract {
    fn default() -> Self {
        Self {
            tournaments: UnorderedMap::new(StorageKeys::Accounts),
        }
    }
}

// Implement the contract structure
#[near_bindgen]
impl Contract {
    fn check_permission() {
        let signer = env::signer_account_id();
        let allow_list = vec!["user1.v2349.testnet", "aei-admin.testnet"];
        if !allow_list.contains(&signer.as_str()) {
            env::panic_str("Error: permission denied");
        }
    }

    #[payable]
    pub fn create_tournament(&mut self, tournament_id: TournamentId, prize_payout_scheme: Vec<JsonPrizePlace>) {
        self::Contract::check_permission();

        if env::attached_deposit() == 0 {
            env::panic_str("Error: no tokens attached");
        }

        match self.tournaments.get(&tournament_id) {
            Some(tournament) if tournament.status != "created".to_string() => {
                env::panic_str(&*format!("Error: Tournament status is already {}", tournament.status));
            }
            _ => (),
        }

        let signer = env::signer_account_id();

        let mut new_tournament = Tournament::default();
        new_tournament.source_wallet = signer.to_string();

        for prize_place in &prize_payout_scheme {
            new_tournament.total_prize_announced += prize_place.team_prize_base_amount.0;
            new_tournament
                .prize_payout_scheme
                .insert(prize_place.player_position, prize_place.team_prize_base_amount.0);
        }

        new_tournament.total_prize_reserved = env::attached_deposit();
        new_tournament.tournament_id = tournament_id;
        new_tournament.status = "created".to_string();

        Promise::new(env::current_account_id()).transfer(new_tournament.total_prize_reserved);
        self.tournaments.insert(&new_tournament.tournament_id, &new_tournament);
    }

    pub fn cancel_tournament(&mut self, tournament_id: TournamentId) {
        self::Contract::check_permission();

        match self.tournaments.get(&tournament_id) {
            Some(mut tournament) if tournament.status == "created".to_string() => {
                tournament.status = "cancelled".to_string();
                Promise::new(AccountId::try_from(tournament.source_wallet.clone()).unwrap()).transfer(tournament.total_prize_reserved);
                self.tournaments.insert(&tournament_id, &tournament);
            }
            Some(tournament) => {
                env::panic_str(&*format!("Error: Cannot cancel tournament with status {}", tournament.status));
            }
            _ => env::panic_str("Error: Tournament is not found"),
        }
    }

    pub fn get_tournament_status(self, tournament_id: TournamentId) -> String {
        match self.tournaments.get(&tournament_id) {
            Some(tournament) => tournament.status,
            None => "Tournament is not found".to_string(),
        }
    }

    pub fn set_participant_list(&mut self, tournament_id: TournamentId, participants: Vec<AccountId>) {
        self::Contract::check_permission();

        match self.tournaments.get(&tournament_id) {
            Some(mut tournament) if tournament.status == "created".to_string() => {
                tournament.status = "started".to_string();
                tournament.participant_list = participants;
                self.tournaments.insert(&tournament_id, &tournament);
            }
            Some(tournament) => {
                env::panic_str(&*format!("Error: Tournament status is already {}", tournament.status));
            }
            _ => env::panic_str("Error: Tournament is not found"),
        }
    }

    pub fn set_winner_list(&mut self, tournament_id: TournamentId, winners: Vec<Winner>) {
        self::Contract::check_permission();

        match self.tournaments.get(&tournament_id) {
            Some(mut tournament) if tournament.status == "started".to_string() => {
                tournament.winner_list = winners;
                let mut payouts_payed: u128 = 0;
                for winner_team in &tournament.winner_list {
                    match tournament.prize_payout_scheme.get(&winner_team.place) {
                        Some(team_prize_base_amount) => {
                            let prize_per_person = (team_prize_base_amount.clone() as f64
                                * winner_team.prize_multiplier_perk
                                / winner_team.accounts.iter().count() as f64)
                                as YoctoNear;
                            for winner_account in &winner_team.accounts {
                                if tournament.participant_list.contains(&winner_account) {
                                    Promise::new(winner_account.clone()).transfer(prize_per_person.clone());
                                    payouts_payed += prize_per_person;
                                }
                            }
                        }
                        _ => env::panic_str("Error: Prize payout scheme is not found"),
                    }
                }
                tournament.status = "finished".to_string();
                if tournament.total_prize_reserved > payouts_payed {
                    let spare_change: u128 = tournament.total_prize_reserved - payouts_payed;
                    Promise::new(AccountId::try_from(tournament.source_wallet.clone()).unwrap()).transfer(spare_change);
                }
                self.tournaments.insert(&tournament_id, &tournament);
            }
            Some(tournament) => {
                env::panic_str(&*format!("Error: Tournament status is already {}", tournament.status));
            }
            _ => env::panic_str("Error: Tournament is not found"),
        }
    }
}
